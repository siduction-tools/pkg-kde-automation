ABOUT
=====

This collection of scripts is codenamed T-101 and its ultimate goal is to reduce
the manual intervention in packaging as much as possible. I would be nice if at
some point in the future, this software would morph into a cyborg meant to replace
software developers ...

                     <((((((\\\
                     /      . }\
                     ;--..--._|}
  (\                 '--/\--'  )
   \\                | '-'  :'|
    \\               . -==- .-|
     \\               \.__.'   \--._
     [\\          __.--|       //  _/'--.
     \ \\       .'-._ ('-----'/ __/      \
      \ \\     /   __>|      | '--.       |
       \ \\   |   \   |     /    /       /
        \ '\ /     \  |     |  _/       /
         \  \       \ |     | /        /
   snd    \  \      \        /

... either by their own will or by force.


INSTALLATION
============

1. Clone this repository in /usr/local/kdenext-automation
2. Add /usr/local/kdenext-automation/ to your $PATH
3. Install the following dependencies:
   - devscripts
   - pkg-kde-tools
   - python
   - python-debian
   - python3-debian


USAGE
=====

Example to build KDE Frameworks 5.12

1. Edit configuration.py and set the appropriate values.

2. Download the tarballs with:
   user@host:~$ download-tarballs-frameworks

3. Update the package names list with:
   user@host:/usr/local/kdenext-automation$ /package-name-list -d siduction -r frameworks -v 5.12

4. Make a directory and clone all the git repositories in it:
   user@host:~$ mkdir kdeframeworks
   user@host:~$ cd kdeframeworks
   user@host:~/kdeframeworks$ git-clone-frameworks-all

5. Checkout the git branch of your choice:
   user@host:~/kdeframeworks$ do-all "git checkout kubuntu_wily_archive"

6. Generate the build depends map with:
   user@host:~/kdeframeworks$ generate_frameworks_dev_map

7. Apply the changes on the packages with:
   user@host:~/kdeframeworks$ do-all prepare-new-release-frameworks

8. Build the source packages with:
   user@host:~/kdeframeworks$ do-all buildsource
   ... and you can check you aren't missing any with:
   user@host:~/kdeframeworks$ do-all check-changes

9. Upload the source packages with:
   user@host:~/kdeframeworks$ do-all uploadsource
   ... and you can check you aren't missing any with:
   user@host:~/kdeframeworks$ do-all check-upload

