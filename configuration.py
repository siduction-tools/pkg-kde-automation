
configuration_siduction = {

	"PKGKDEAUTOMATION_INSTALL_DIR":'/usr/local/kdenext-automation/',

	"PKGKDETOOLSVERSION":"0.15.19+siduction1",

	"TARGET_DIST":"kdenext",

	# KDE Frameworks
	"FRAMEWORKS_VERSION":'5.15',
	"FRAMEWORKS_PACKAGE_REV":'siduction1',
	"FRAMEWORKS_TARBALLS_DIR":'/home/santa/kde-ftp/frameworks',

	# KDE's Plasma 5
	"PLASMA_VERSION":'5.4.2',
	"PLASMA_PACKAGE_REV":'siduction1',
	"PLASMA_TARBALLS_DIR":'/home/santa/kde-ftp/plasma-next',
	"BASEDIR_PLASMA":'/home/santa/kde-ftp/plasma-next',

        # KDE's Applications
	"APPS_VERSION":'15.08.2',
	"APPS_PACKAGE_REV":'siduction1',
	"APPS_TARBALLS_DIR":'/home/santa/kde-ftp/applications',

	# Qt 5
	"QT5_VERSION":'5.5.0',
	"QT5_PACKAGE_REV":'1~siduction1',
	"QT5_TARBALLS_DIR":'/home/santa/kde-ftp/qt5',

	# Packages lists
	"QT5_PACKAGE_LIST":'/usr/local/kdenext-automation/package-name-lists/qt5-siduction',
	"FRAMEWORKS_PACKAGE_LIST":'/usr/local/kdenext-automation/package-name-lists/frameworks-siduction',
	"PLASMA_PACKAGE_LIST":'/usr/local/kdenext-automation/package-name-lists/plasma-siduction',
	"APPS_PACKAGE_LIST":'/usr/local/kdenext-automation/package-name-lists/applications-siduction'
}

configuration_ubuntu = {

	"PKGKDEAUTOMATION_INSTALL_DIR":'/usr/local/kdenext-automation/',

	"PKGKDETOOLSVERSION":"0.15.18+siduction1",

	"TARGET_DIST":"ubuntu-exp",

	# KDE Frameworks
	"FRAMEWORKS_VERSION":'5.13',
	"FRAMEWORKS_PACKAGE_REV":'~santa5.',
	"FRAMEWORKS_TARBALLS_DIR":'/home/santa/kde-ftp/frameworks',

	# KDE's Plasma 5
	"PLASMA_VERSION":'5.3.2',
	"PLASMA_PACKAGE_REV":'+santa4.',
	"PLASMA_TARBALLS_DIR":'/home/santa/kde-ftp/plasma-next',
	"BASEDIR_PLASMA":'/home/santa/kde-ftp/plasma-next',

        # KDE's Applications
	"APPS_VERSION":'15.08.2',
	"APPS_PACKAGE_REV":'+santa1.',
	"APPS_TARBALLS_DIR":'/home/santa/kde-ftp/applications',

	# Qt 5
	"QT5_VERSION":'5.4.2',
	"QT5_PACKAGE_REV":'siduction2',

	# Packages lists
	"QT5_PACKAGE_LIST":'/usr/local/kdenext-automation/package-name-lists/qt5-siduction',
	"FRAMEWORKS_PACKAGE_LIST":'/usr/local/kdenext-automation/package-name-lists/frameworks-siduction',
	"PLASMA_PACKAGE_LIST":'/usr/local/kdenext-automation/package-name-lists/plasma-siduction',
	"APPS_PACKAGE_LIST":'/usr/local/kdenext-automation/package-name-lists/applications-siduction'
}

configuration = configuration_siduction
#configuration = configuration_ubuntu
