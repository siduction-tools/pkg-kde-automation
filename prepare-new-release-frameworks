#!/bin/sh

#DCH="dch --release-heuristic=log"
DCH="create-new-changelog-entry"

SRCPKG=`dpkg-parsechangelog | grep ^Source: | sed -e 's/Source: //'`

export `print-configuration`

#Before anything, sanitize debian/control:
sanitize-control-file

#Prepare changelog
#${DCH} -b --newversion \
#	$(find-latest-version ${FRAMEWORKS_TARBALLS_DIR}/${FRAMEWORKS_VERSION})-${FRAMEWORKS_PACKAGE_REV} \
#	--distribution ${TARGET_DIST} --force-distribution\
#	"Release for kdenext."
create-new-changelog-block -r frameworks

#Adjust Maintainer and Uploaders field.
${DCH} "Adjust Maintainer and Uploaders field."
adjust-maintainer-uploaders

#Adjust Vcs-* fields
adjust-vcs-fields
${DCH} "Adjust Vcs-* fields."

#Bump pkg-kde-tools B-D
bump-pkg-kde-tools-bd ${PKGKDETOOLSVERSION}
${DCH} "Bump pkg-kde-tools build dependency to ${PKGKDETOOLSVERSION}."

#Bump Qt 5 Build-Depends.
#bump-qt5-bd
#${DCH} "Bump Qt 5 build dependencies."

#Bump KDE Frameworks build dependencies.
bump-frameworks-bd
${DCH} "Bump KDE Frameworks build dependencies."

#Include runtime dependencies via symbols files
inject-runtime-deps-symbols
git add debian/*.lintian-overrides
${DCH} "Inject runtime dependencies in symbols files if any."

#Exclude runtime packages from dh_shlibdeps in order to avoid circular dependencies.
case $SRCPKG in
kio | kross)
	exclude-runtime-dh_shlibdeps $SRCPKG
	${DCH} "Exclude runtime package from dh_shlibdeps."
	;;
solid)
	exclude-runtime-dh_shlibdeps "libkf5solid-bin"
	${DCH} "Exclude runtime package from dh_shlibdeps."
	;;
kconfig)
	exclude-runtime-dh_shlibdeps "libkf5config-bin"
	${DCH} "Exclude runtime package from dh_shlibdeps."
	;;
kglobalaccel)
	exclude-runtime-dh_shlibdeps "libkf5globalaccel-bin"
	${DCH} "Exclude runtime package from dh_shlibdeps."
	;;
*)
	;;
esac


#Get tarball
get-tarball-frameworks

#Get upstream version
UVERSION=`dpkg-parsechangelog | grep ^Version: | sed -e 's/Version: //;s/.*\://;s/-.*$//'`

#kactivities needs a couple of changes to upgrade nicely from KDE SC 4
if [ ${SRCPKG} = "kactivities-kf5" ]; then
	fix-kactivities-relationships
	${DCH} "Add libkactivities-bin dummy package."
	${DCH} "In kactivities drop conflict against libkactivities-bin, instead break/replace libkactivities-bin (<< 4:5.0.0)"
	${DCH} "Fix qml-* Breaks/Replaces."
	fix-libkactivities-bin-version
	${DCH} "Modify debian/rules so the libkactivities-bin dummy package will have the right version."
fi

if [ ${SRCPKG} = "plasma-framework" ]; then
	fix-plasma-framework-control
	${DCH} "Add libegl1-mesa-dev to Build-Depends."
fi

if [ ${SRCPKG} = "kglobalaccel" ]; then
	fix-kglobalaccel-relationships
	${DCH} "Fix Breaks/Replaces, set the right versions for kdenext."
fi

#kapidox needs the source unpacked.
if [ ${SRCPKG} = "kapidox" ]; then
	git clean -xdff
	tar xvf ../kapidox_${UVERSION}.orig.tar.xz --strip=1
fi

#There are several packages needing a replaces against kde-l10n-*
case $SRCPKG in
kde-cli-tools | plasma-desktop | kwin | kio-extras | libksysguard | ksysguard | plasma-workspace | \
khelpcenter | oxygen | khotkeys | kinfocenter | powerdevil | systemsettings | kdeplasma-addons | \
kmenuedit | kfilemetadata-kf5 | baloo-kf5)
	${DCH} "Add Replaces against kde-l10n-* where needed."
	add-replaces-against-kde-l10n
	;;
*)
	;;
esac

#Some packages with only arch all packages in control need a dummy
#arch any package to fool kdenext's buildd
#if [ ${SRCPKG} = "kapidox" ]; then
#	cancamusa "kapidox"
#fi

#kactivities needs the sqlite plugin for Qt Sql.
if [ ${SRCPKG} = "kactivities-kf5" ]; then
        add-sqlite-plugin-depends
	${DCH} "Make kactivities depend on libqt5sql5-sqlite."
fi

case $SRCPKG in
knewstuff | kxmlrpcclient )
	pass-c0-gensymbols --dhmk
	${DCH} "Pass -c0 to dpkg-gensymbols."
	;;
*)
	;;
esac

#Temporary solution for this
#http://git.debian.org/?p=pkg-kde/frameworks/kservice.git;a=commitdiff;h=7d248e5
#while kubuntu doesn't merge from debian
if [ ${SRCPKG} = "kservice" ]; then
        add-replaces-kservice
	${DCH} "Add replaces against libkf5service-bin."
fi

#Close the changelog
#${DCH} -r --dist ${TARGET_DIST} --force-distribution ""
